import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input13.txt", "utf-8"))
  .trim()
  .split("\n\n")
  .map((x) => x.split("\n").map((x) => JSON.parse(x)));

console.log(
  input
    .map(([first, second], index) => {
      return compare(first, second) === -1 ? index + 1 : 0;
    })
    .map((x) => {
      console.log(x > 0 && "right order");
      return x;
    })
    .reduce((acc, x) => acc + x, 0)
);

function compare(first, second, indent = "") {
  console.log(
    `${indent}comparing ${JSON.stringify(first)} vs ${JSON.stringify(second)}`
  );
  if (!_.isArray(first) && !_.isArray(second)) {
    if (first === second) {
      return 0;
    }
    if (first < second) {
      return -1;
    }
    return 1;
  }
  if (_.isArray(first) && _.isArray(second)) {
    for (let i = 0; i < Math.min(first.length, second.length); i++) {
      switch (compare(first[i], second[i], indent + "  ")) {
        case 1:
          return 1;
        case -1:
          return -1;
      }
    }
    if (first.length === second.length) {
      return 0;
    }
    if (first.length < second.length) {
      return -1;
    }
    if (first.length > second.length) {
      return 1;
    }
  }
  if (!_.isArray(first)) {
    return compare([first], second, indent + "  ");
  }
  if (!_.isArray(second)) {
    return compare(first, [second], indent + "  ");
  }
}
