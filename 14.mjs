import { readFile } from "fs/promises";
import _ from "lodash";

let bounds = [
  [1000, 0],
  [1000, 0],
];
function growbounds([x, y]) {
  bounds[0][0] = Math.min(bounds[0][0], x);
  bounds[0][1] = Math.max(bounds[0][1], x);
  bounds[1][0] = Math.min(bounds[1][0], y);
  bounds[1][1] = Math.max(bounds[1][1], y);
  return [x, y];
}

const input = (await readFile("data/input14.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((x) =>
    x.split(" -> ").map((x) => growbounds(x.split(",").map((x) => parseInt(x))))
  );

growbounds([500, 0]);
bounds[0][0]--;
bounds[1][0]--;
bounds[0][1]++;
bounds[1][1]++;

const grid = new Array(bounds[1][1] - bounds[1][0] + 1)
  .fill(0)
  .map((x) =>
    new Array(bounds[0][1] - bounds[0][0] + 1).fill(0).map((x) => ".")
  );

function set([x, y], val = "#") {
  grid[y - bounds[1][0]][x - bounds[0][0]] = val;
}
function empty([x, y]) {
  return grid[y - bounds[1][0]][x - bounds[0][0]] == ".";
}

input.forEach(([cursor, ...rest]) => {
  for (let p of rest) {
    set(cursor);
    while (!_.isEqual(cursor, p)) {
      const d = [p[0] - cursor[0], p[1] - cursor[1]];
      // normalize
      cursor[0] += d[0] / Math.abs(d[0]) || 0;
      cursor[1] += d[1] / Math.abs(d[1]) || 0;
      set(cursor);
    }
  }
});

set([500, 0], "+");

for (let i = 1; i <= 1000; i++) {
  let sand = [500, 0];
  console.log(sand, i);
  while (true) {
    if (empty([sand[0], sand[1] + 1])) {
      sand[1]++;
    } else if (empty([sand[0] - 1, sand[1] + 1])) {
      sand[0]--;
      sand[1]++;
    } else if (empty([sand[0] + 1, sand[1] + 1])) {
      sand[0]++;
      sand[1]++;
    } else {
      break;
    }
  }
  set(sand, "o");
}

console.log(grid.map((line) => line.join("")).join("\n"));

console.log(bounds);
