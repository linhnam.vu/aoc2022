import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input11.txt", "utf-8"))
  .trim()
  .split("\n\n")
  .map((x) => x.split("\n"))
  .map(([name, startingLine, operation, test, trueOp, falseOp], index) => {
    const starting = startingLine
      .split(":")[1]
      .split(", ")
      .map((x) => +x);
    const [op, arg] = operation.substring(23).split(" ");
    return {
      items: starting,
      operation:
        arg === "old"
          ? (n) => n * n
          : op === "*"
          ? (n) => n * +arg
          : (n) => n + +arg,
      test: +test.substring(21),
      trueOp: +trueOp.substring(29),
      falseOp: +falseOp.substring(30),
      count: 0,
    };
  });

for (let round = 0; round < 20; round++) {
  for (let monkey of input) {
    const { items, operation, test, trueOp, falseOp } = monkey;
    for (let i of items) {
      const worry = Math.floor(operation(i) / 3);
      console.log(i, worry, operation.toString());
      if (worry % test == 0) {
        input[trueOp].items.push(worry);
      } else {
        input[falseOp].items.push(worry);
      }
      monkey.count++;
    }
    monkey.items = [];
  }
  console.log(input.map(({ items }) => items.join(",")).join("\n"));
  console.log("===");
}
console.log(
  input
    .map(({ count }) => count)
    .sort((a, b) => b - a)
    .slice(0, 2)
    .reduce((acc, a) => acc * a, 1)
);
// console.log(input);
