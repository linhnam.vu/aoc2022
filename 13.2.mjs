import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input13.txt", "utf-8"))
  .trim()
  .split("\n")
  .filter((x) => x.length)
  .map((x) => JSON.parse(x));

const sorted = [...input, [[2]], [[6]]].sort(compare);
console.log(
  sorted.reduce(
    (acc, x, index) =>
      acc * (_.isEqual(x, [[2]]) || _.isEqual(x, [[6]]) ? index + 1 : 1),
    1
  )
);

function compare(first, second, indent = "") {
  console.log(
    `${indent}comparing ${JSON.stringify(first)} vs ${JSON.stringify(second)}`
  );
  if (!_.isArray(first) && !_.isArray(second)) {
    if (first === second) {
      return 0;
    }
    if (first < second) {
      return -1;
    }
    return 1;
  }
  if (_.isArray(first) && _.isArray(second)) {
    for (let i = 0; i < Math.min(first.length, second.length); i++) {
      switch (compare(first[i], second[i], indent + "  ")) {
        case 1:
          return 1;
        case -1:
          return -1;
      }
    }
    if (first.length === second.length) {
      return 0;
    }
    if (first.length < second.length) {
      return -1;
    }
    if (first.length > second.length) {
      return 1;
    }
  }
  if (!_.isArray(first)) {
    return compare([first], second, indent + "  ");
  }
  if (!_.isArray(second)) {
    return compare(first, [second], indent + "  ");
  }
}
