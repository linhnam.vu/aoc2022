import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input9.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((x) => x.split(" "))
  .map(([dir, amt]) => ({ dir, amt: +amt }));

console.log(input);

let head = { x: 0, y: 0 };
let tail = { x: 0, y: 0 };

function touching(a, b) {
  return Math.abs(a.y - b.y) <= 1 && Math.abs(a.x - b.x) <= 1;
}

const keypress = async () => {
  process.stdin.setRawMode(true);
  return new Promise((resolve) =>
    process.stdin.once("data", (data) => {
      const byteArray = [...data];
      if (byteArray.length > 0 && byteArray[0] === 3) {
        console.log("^C");
        process.exit(1);
      }
      process.stdin.setRawMode(false);
      resolve();
    })
  );
};

const pos = {};
pos[[tail.x, tail.y]] = true;
for (let index = 0; index < input.length; index++) {
  const { dir, amt } = input[index];
  for (let index = 0; index < amt; index++) {
    switch (dir) {
      case "R":
        head.x += 1;
        break;
      case "L":
        head.x -= 1;
        break;
      case "U":
        head.y += 1;
        break;
      case "D":
        head.y -= 1;
        break;
      default:
        break;
    }
    // console.log(head, tail);
    if (touching(head, tail)) {
      continue;
    }
    if (head.x == tail.x) {
      if (tail.y > head.y) {
        tail.y = head.y + 1;
      } else {
        tail.y = head.y - 1;
      }
    } else if (head.y == tail.y) {
      if (tail.x > head.x) {
        tail.x = head.x + 1;
      } else {
        tail.x = head.x - 1;
      }
    } else {
      if (tail.x > head.x) {
        tail.x = tail.x - 1;
      } else {
        tail.x = tail.x + 1;
      }
      if (tail.y > head.y) {
        tail.y = tail.y - 1;
      } else {
        tail.y = tail.y + 1;
      }
    }
    pos[[tail.x, tail.y]] = true;
  }
  console.log(head, tail);
  // await keypress();
}
console.log(pos, Object.keys(pos).length);
