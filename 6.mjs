import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input6.txt", "utf-8")).trim();
// const input = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";

const index = _.range(0, input.length).find(
  (x) => _.uniq(input.substring(x, x + 4)).length == 4
);

console.log(input.substring(index, index + 4));
console.log(index + 4);
