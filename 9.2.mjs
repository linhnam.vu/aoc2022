import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input9.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((x) => x.split(" "))
  .map(([dir, amt]) => ({ dir, amt: +amt }));

console.log(input);

let head = { x: 0, y: 0 };
let tails = _.range(0, 9).map((x) => ({ x: 0, y: 0 }));

function touching(a, b) {
  return Math.abs(a.y - b.y) <= 1 && Math.abs(a.x - b.x) <= 1;
}

const keypress = async () => {
  process.stdin.setRawMode(true);
  return new Promise((resolve) =>
    process.stdin.once("data", (data) => {
      const byteArray = [...data];
      if (byteArray.length > 0 && byteArray[0] === 3) {
        console.log("^C");
        process.exit(1);
      }
      process.stdin.setRawMode(false);
      resolve();
    })
  );
};

const pos = {};
pos[[tails[8].x, tails[8].y]] = true;
for (let index = 0; index < input.length; index++) {
  const { dir, amt } = input[index];
  for (let index = 0; index < amt; index++) {
    switch (dir) {
      case "R":
        head.x += 1;
        break;
      case "L":
        head.x -= 1;
        break;
      case "U":
        head.y += 1;
        break;
      case "D":
        head.y -= 1;
        break;
      default:
        break;
    }
    tails.forEach((tail, index) => {
      const h = index == 0 ? head : tails[index - 1];
      // console.log(head, tail);
      if (touching(h, tail)) {
        return;
      }
      if (h.x == tail.x) {
        if (tail.y > h.y) {
          tail.y = h.y + 1;
        } else {
          tail.y = h.y - 1;
        }
      } else if (h.y == tail.y) {
        if (tail.x > h.x) {
          tail.x = h.x + 1;
        } else {
          tail.x = h.x - 1;
        }
      } else {
        if (tail.x > h.x) {
          tail.x = tail.x - 1;
        } else {
          tail.x = tail.x + 1;
        }
        if (tail.y > h.y) {
          tail.y = tail.y - 1;
        } else {
          tail.y = tail.y + 1;
        }
      }
    });
    pos[[tails[8].x, tails[8].y]] = true;
    console.log(
      _.range(-10, 10)
        .map((x) =>
          _.range(-20, 20)
            .map((y) => {
              if (head.x == x && head.y == y) {
                return "H";
              }
              for (let index = 0; index < tails.length; index++) {
                const tail = tails[index];
                if (tail.x == x && tail.y == y) {
                  return index.toString();
                }
              }
              return ".";
            })
            .join("")
        )
        .join("\n")
    );
    // await keypress();
  }
  //   console.log(head, tail);
}
console.log(pos, Object.keys(pos).length);
