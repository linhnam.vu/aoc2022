import { readFile } from "fs/promises";
import _ from "lodash";
import { markAsUntransferable } from "worker_threads";

const input = (await readFile("data/input15.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((x) => {
    const [pre, post] = x.split(": closest beacon is at x=");
    return [
      pre
        .substring("Sensor at x=".length)
        .split(", y=")
        .map((x) => +x),
      post.split(", y=").map((x) => +x),
    ];
  });

function dist(a, b) {
  return Math.abs(a[0] - b[0]) + Math.abs(a[1] - b[1]);
}

const MAX = 4000000;
let found = 0;
function check(y) {
  const regions = [];
  input.forEach(([sensor, beacon]) => {
    const d = dist(sensor, beacon);
    const od = dist(sensor, [sensor[0], y]);
    if (od <= d) {
      regions.push([sensor[0] - d + od, sensor[0] + d - od]);
    }
  });

  const markers = _.uniq([
    0,
    MAX,
    ...regions
      .reduce((acc, [a, b]) => [...acc, a, b + 1], [])
      .filter((x) => x >= 0 && x <= MAX),
  ]).sort((a, b) => a - b);
  //   console.log(markers, regions);
  return markers
    .slice(1)
    .map((end, index) => {
      const start = markers[index];
      if (regions.some(([a, b]) => start >= a && start <= b)) {
        // console.log(start, end, end - start);
        return end - start;
      }
      found = start;
      return 0;
    })
    .reduce((acc, c) => acc + c, 0);
}

for (let i = 0; i <= MAX; i++) {
  const c = check(i);
  if (c != MAX) {
    console.log(found * 4000000 + i);
  }
}
