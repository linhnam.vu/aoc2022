import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input8.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((x) => x.split("").map((x) => +x));

function highestX(x, a, b, c) {
  const d = a > b ? -1 : 1;
  let h = 0;
  for (let i = a; i >= 0 && i < HEIGHT && i != b + d; i += d) {
    if (input[x][i] >= c) {
      h++;
      return h;
    }
    h++;
  }
  return h;
}
function highestY(y, a, b, c) {
  const d = a > b ? -1 : 1;
  let h = 0;
  for (let i = a; i >= 0 && i < WIDTH && i != b + d; i += d) {
    if (input[i][y] >= c) {
      h++;
      return h;
    }
    h++;
  }
  return h;
}
let answer = 0;
const HEIGHT = input.length;
const WIDTH = input[0].length;
console.log(
  input
    .map((row, x) => {
      return row
        .map((c, y) => {
          let score = [];
          score.push(highestX(x, y - 1, 0, c));
          score.push(highestX(x, y + 1, HEIGHT - 1, c));
          score.push(highestY(y, x - 1, 0, c));
          score.push(highestY(y, x + 1, WIDTH - 1, c));
          if (score.reduce((acc, x) => x * acc, 1) > answer)
            answer = score.reduce((acc, x) => x * acc, 1);
          return score.toString();
        })
        .join(" ");
    })
    .join("\n")
);
console.log(answer);
