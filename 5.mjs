import { readFile } from "fs/promises";
import _ from "lodash";

const [starting, operations] = (await readFile("data/input5.txt", "utf-8"))
  .trim()
  .split("\n\n");

const ss = starting
  .split("\n")
  .slice(0, -1)
  .map((x) => _.range(0, 9).map((c) => x.charAt(c * 4 + 1)));

const stacks = {};
ss.forEach((line) =>
  line.forEach((item, c) => {
    if (item == " ") return;
    const column = c + 1;
    stacks[column] = stacks[column] || [];
    stacks[column].unshift(item);
  })
);

// console.log(stacks);

operations
  .split("\n")
  .map((line) => line.split(" "))
  .map((line) => [+line[1], +line[3], +line[5]])
  .forEach(([move, from, to]) => {
    const carrying = [];
    for (let index = 0; index < move; index++) {
      carrying.unshift(stacks[from].pop());
    }
    stacks[to].push(...carrying);
  });

console.log(_.values(_.mapValues(stacks, (s) => s.slice(-1))).join(""));
