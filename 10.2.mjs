import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input10.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((x) => x.split(" "))
  .map(([op, amt]) => ({ op, amt: +amt }));

console.log(input);
const buffer = _.range(6).map((x) => _.range(40).map((x) => " "));

let value = 1;
const state = [0];
let answer = 0;
let counter = 0;
input.forEach(({ op, amt }, index) => {
  let cycles = 1;
  switch (op) {
    case "noop":
      break;
    case "addx":
      cycles = 2;
      state[1] = (state[1] ?? 0) + amt;
  }
  for (let i = 0; i < cycles; i++) {
    console.log(counter, value);

    if (Math.abs(value - (counter % 40)) <= 1) {
      buffer[Math.floor((counter % 240) / 40)][counter % 40] = "#";
    }
    console.log(buffer.map((l) => l.join("")).join("\n"));
    counter++;

    // if ((counter - 20) % 40 == 0) {
    //   answer += value * counter;
    // //   console.log(counter, value, state, value * counter, answer);
    // } else {
    // //   console.log(counter, value, state);
    // }

    let acc = state.splice(0, 1)[0] ?? 0;
    value += acc;
  }
  //   console.log(state, value);
});
