import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input7.txt", "utf-8")).trim().split("$ ");

let path = [];
let tree = {};
input.forEach((stuff) => {
  const [cmdline, ...output] = stuff.trim().split("\n");
  const [cmd, args] = cmdline.split(" ");
  if (cmd === "cd") {
    if (args === "/") {
      path = [];
      return;
    }
    if (args === "..") {
      path.pop();
      return;
    }
    path.push(args);
    return;
  } else {
    const parsed = output
      .filter((x) => !x.startsWith("dir"))
      .map((x) => {
        const [size, file] = x.split(" ");
        return { file, size: +size };
      });
    _.set(tree, [...path, "."], parsed);
    console.log(
      parsed.reduce((acc, x) => acc + x.size, 0),
      output
    );
    // _.set(
    //   tree,
    //   [...path, ".size"],
    //   parsed.reduce((acc, x) => acc + x.size, 0)
    // );
    // console.log([cmd, args, output]);
  }
});

let answer = 0;
const answers = [];
function walk(node, fullpath = []) {
  let sum = 0;
  Object.keys(node).forEach((path) => {
    sum += node["."].reduce((acc, x) => acc + x.size, 0);
    if (path.startsWith(".")) return;
    sum += walk(node[path], [...fullpath, path]);
  });
  console.log(fullpath, sum);
  if (sum <= 100000) {
    answer += sum;
  }
  answers.push(`${sum} ${fullpath.join("/")} `);
  return sum;
}

walk(tree);
console.log(answers.sort().join("\n"));
console.log(tree);
console.log(answer);
