import { readFile } from "fs/promises";
import _ from "lodash";

const lines = (await readFile("data/input2.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((x) => x.split(" "))
  .map(([opp, strat]) => {
    const you = {
      A: { X: "Z", Y: "X", Z: "Y" },
      B: { X: "X", Y: "Y", Z: "Z" },
      C: { X: "Y", Y: "Z", Z: "X" },
    }[opp][strat];
    const win = {
      A: { X: 3, Y: 6, Z: 0 },
      B: { X: 0, Y: 3, Z: 6 },
      C: { X: 6, Y: 0, Z: 3 },
    };
    return { X: 1, Y: 2, Z: 3 }[you] + win[opp][you];
  })
  .reduce((acc, x) => acc + x, 0);

console.log(lines);
