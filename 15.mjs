import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input15.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((x) => {
    const [pre, post] = x.split(": closest beacon is at x=");
    return [
      pre
        .substring("Sensor at x=".length)
        .split(", y=")
        .map((x) => +x),
      post.split(", y=").map((x) => +x),
    ];
  });

function dist(a, b) {
  return Math.abs(a[0] - b[0]) + Math.abs(a[1] - b[1]);
}

const grid = {};
let count = 0;
const regions = [];
input.forEach(([sensor, beacon]) => {
  const d = dist(sensor, beacon);
  grid[sensor] = "S";
  grid[beacon] = "B";
  const od = dist(sensor, [sensor[0], 2000000]);
  if (od <= d) {
    // console.log(sensor, od, d);
    regions.push([sensor[0] - d + od, sensor[0] + d - od]);
  }
});

const markers = _.uniq(
  regions.reduce((acc, [a, b]) => [...acc, a, b], [])
).sort((a, b) => a - b);
console.log(
  markers
    .slice(1)
    .map((end, index) => {
      const start = markers[index];
      if (regions.some(([a, b]) => start >= a && start <= b)) {
        return end - start;
      }
      console.log(start, end, end - start);
    })
    .reduce((acc, c) => acc + c, 0)
);
