import { readFile } from "fs/promises";
import _ from "lodash";

const lines = (await readFile("data/input3.txt", "utf-8"))
  .split("\n")
  .map((line) => {
    const contents = line.split("");
    const half = contents.length / 2;
    return [contents.slice(0, half), contents.slice(half)];
  })
  .map(([a, b]) => {
    return _.intersection(a, b);
  })
  .map((common) => {
    return common.map((c) => {
      const cc = c.charCodeAt(0);
      if (cc >= 97) return cc - 97 + 1;
      return cc - 65 + 27;
    });
  })
  .map((p) => _.sum(p));
console.log(_.sum(lines));
