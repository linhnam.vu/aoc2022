import { readFile } from "fs/promises";
import _ from "lodash";

const lines = (await readFile("data/input1.txt", "utf-8"))
  .split("\n\n")
  .map((set) => set.split("\n").map((x) => parseInt(x)))
  .map((cal, index) => ({ index, sum: cal.reduce((acc, x) => acc + x, 0) }));

console.log(_.sortBy(lines, (x) => -x.sum));
