import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input6.txt", "utf-8")).trim();
// const input = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";

const index = _.range(0, input.length).find(
  (x) => _.uniq(input.substring(x, x + 14)).length == 14
);

console.log(input.substring(index, index + 14));
console.log(index + 14);
