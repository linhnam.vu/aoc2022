import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input12.txt", "utf-8")).trim().split("\n");

const grid = {};
let start, end;
input.forEach((line, y) => {
  line.split("").map((c, x) => {
    if (c === "S") {
      grid[[x, y]] = 0;
      start = [x, y];
    } else if (c === "E") {
      grid[[x, y]] = 25;
      end = [x, y];
    } else {
      grid[[x, y]] = c.charCodeAt(0) - "a".charCodeAt(0);
    }
  });
});

const WIDTH = 178;
const HEIGHT = 40;
console.log(
  _.range(HEIGHT)
    .map((y) =>
      _.range(WIDTH)
        .map((x) => String.fromCharCode("a".charCodeAt(0) + grid[[x, y]]))
        .join("")
    )
    .join("\n")
);

function neighbors([x, y]) {
  const neighbors = [];
  if (x != 0) {
    neighbors.push([x - 1, y]);
  }
  if (x != WIDTH - 1) {
    neighbors.push([x + 1, y]);
  }
  if (y != 0) {
    neighbors.push([x, y - 1]);
  }
  if (x != HEIGHT - 1) {
    neighbors.push([x, y + 1]);
  }
  const h = grid[[x, y]];
  return neighbors.filter((n) => {
    return grid[n] >= h - 1;
  });
}

const dist = {};
const prev = {};

dist[end] = 0;

const queue = _.fromPairs(Object.keys(grid).map((x) => [x, true]));

while (Object.keys(queue).length) {
  const best = _.chain(Object.keys(queue))
    .sortBy((p) => dist[p] ?? Number.POSITIVE_INFINITY)
    .first()
    .value();

  delete queue[best];
  for (const neighbor of neighbors(best.split(",").map((x) => +x))) {
    const value = dist[best] + 1;
    if (value < (dist[neighbor] ?? Number.POSITIVE_INFINITY)) {
      dist[neighbor] = value;
      prev[neighbor] = best;
    }
  }
  //   console.log(queue, best, dist[best]);
  //   break;
}

console.log(
  Object.keys(grid)
    .filter((pos) => grid[pos] == 0)
    .map((pos) => dist[pos])
    .sort()
);

console.log(dist[start]);

// console.log(neighbors(start));
