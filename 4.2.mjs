import { readFile } from "fs/promises";
import _ from "lodash";

const original = (await readFile("data/input4.txt", "utf-8")).split("\n");
const lines = (await readFile("data/input4.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((line) =>
    line
      .split(",")
      .map((x) => x.split("-").map((x) => +x))
      .map(([a, b]) => _.range(a, b + 1))
  )
  .map(([a, b], index) => {
    const overlap = _.intersection(a, b);
    return overlap.length > 0;
  });

console.log(lines.filter((x) => x).length);
