import { readFile } from "fs/promises";
import _ from "lodash";

const input = (await readFile("data/input8.txt", "utf-8"))
  .trim()
  .split("\n")
  .map((x) => x.split("").map((x) => +x));

function highestX(x, a, b) {
  let h = -1;
  for (let i = a; i < b; i++) {
    if (input[x][i] > h) {
      h = input[x][i];
    }
  }
  return h;
}
function highestY(y, a, b) {
  let h = -1;
  for (let i = a; i < b; i++) {
    if (input[i][y] > h) {
      h = input[i][y];
    }
  }
  return h;
}
let answer = 0;
const HEIGHT = input.length;
const WIDTH = input[0].length;
console.log(
  input
    .map((row, x) => {
      return row
        .map((c, y) => {
          if (highestX(x, 0, y) < c) {
            answer += 1;
            return "+" + c;
          }
          if (highestX(x, y + 1, HEIGHT) < c) {
            answer += 1;
            return "+" + c;
          }
          if (highestY(y, 0, x) < c) {
            answer += 1;
            return "+" + c;
          }
          if (highestY(y, x + 1, WIDTH) < c) {
            answer += 1;
            return "+" + c;
          }
          return " " + c;
        })
        .join("");
    })
    .join("\n")
);
console.log(answer);
